Rails.application.routes.draw do
  resources :marks
  resources :rms
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
