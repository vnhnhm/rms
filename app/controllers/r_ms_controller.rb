class RmsController < ApplicationController
  before_action :set_rm, only: [:show, :edit, :update, :destroy]

  # GET /rms
  # GET /rms.json
  def index
    @rms = Rm.all
  end

  # GET /rms/1
  # GET /rms/1.json
  def show
  end

  # GET /rms/new
  def new
    @rm = Rm.new
  end

  # GET /rms/1/edit
  def edit
  end

  # POST /rms
  # POST /rms.json
  def create
    @rm = Rm.new(rm_params)

    respond_to do |format|
      if @rm.save
        format.html { redirect_to @rm, notice: 'Rm was successfully created.' }
        format.json { render :show, status: :created, location: @rm }
      else
        format.html { render :new }
        format.json { render json: @rm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rms/1
  # PATCH/PUT /rms/1.json
  def update
    respond_to do |format|
      if @rm.update(rm_params)
        format.html { redirect_to @rm, notice: 'Rm was successfully updated.' }
        format.json { render :show, status: :ok, location: @rm }
      else
        format.html { render :edit }
        format.json { render json: @rm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rms/1
  # DELETE /rms/1.json
  def destroy
    @rm.destroy
    respond_to do |format|
      format.html { redirect_to rms_url, notice: 'Rm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rm
      @rm = Rm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rm_params
      params.require(:rm).permit(:user_id, :content, :quantity, :max)
    end
end
