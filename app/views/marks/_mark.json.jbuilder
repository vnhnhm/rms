json.extract! mark, :id, :user_id, :content, :quantity, :max, :created_at, :updated_at
json.url mark_url(mark, format: :json)
