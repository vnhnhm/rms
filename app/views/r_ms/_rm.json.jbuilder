json.extract! rm, :id, :user_id, :content, :quantity, :max, :created_at, :updated_at
json.url rm_url(rm, format: :json)
