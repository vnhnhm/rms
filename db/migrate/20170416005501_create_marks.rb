class CreateMarks < ActiveRecord::Migration[5.0]
  def change
    create_table :marks do |t|
      t.references :user, foreign_key: true
      t.string :content
      t.string :quantity
      t.string :max

      t.timestamps
    end
  end
end
