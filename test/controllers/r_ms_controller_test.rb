require 'test_helper'

class RMsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rm = rms(:one)
  end

  test "should get index" do
    get rms_url
    assert_response :success
  end

  test "should get new" do
    get new_rm_url
    assert_response :success
  end

  test "should create rm" do
    assert_difference('Rm.count') do
      post rms_url, params: { rm: { content: @rm.content, max: @rm.max, quantity: @rm.quantity, user_id: @rm.user_id } }
    end

    assert_redirected_to rm_url(Rm.last)
  end

  test "should show rm" do
    get rm_url(@rm)
    assert_response :success
  end

  test "should get edit" do
    get edit_rm_url(@rm)
    assert_response :success
  end

  test "should update rm" do
    patch rm_url(@rm), params: { rm: { content: @rm.content, max: @rm.max, quantity: @rm.quantity, user_id: @rm.user_id } }
    assert_redirected_to rm_url(@rm)
  end

  test "should destroy rm" do
    assert_difference('Rm.count', -1) do
      delete rm_url(@rm)
    end

    assert_redirected_to rms_url
  end
end
